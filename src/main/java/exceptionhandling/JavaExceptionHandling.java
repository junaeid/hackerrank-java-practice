package exceptionhandling;

public class JavaExceptionHandling {
    public static void main(String[] args) throws Exception {
        int n = 3;
        int p = 5;
        JavaExceptionHandling javaExceptionHandling = new JavaExceptionHandling();
        System.out.println(javaExceptionHandling.power(n, p));
    }

    /**
     * Each line of the input contains two integers, n and p. If both are positive then return the power of two.
     * Otherwise, if either one of them is negative
     * then throws exception message contains "n and p should be non-negative"
     * If both are zero, the output contains "n and p should not be zero".
     *
     * @param n Int
     * @param p Int
     * @return Int: power of two numbers
     * @throws Exception "n and p should be non-negative" or "n and p should not be zero"
     */
    public long power(int n, int p) throws Exception {
        if (n == 0 && p == 0)
            throw new Exception("n and p should not be zero.");
        else if (n < 0 || p < 0)
            throw new Exception("n or p should not be negative.");
        return (long) Math.pow(n, p);
    }
}
