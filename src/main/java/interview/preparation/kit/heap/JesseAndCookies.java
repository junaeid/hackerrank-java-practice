package interview.preparation.kit.heap;

import java.util.*;

public class JesseAndCookies {
    public static void main(String[] args) {
        int k = 7;
        List<Integer> arr = new ArrayList<>(Arrays.asList(1, 2, 3, 9, 10, 12));

        System.out.println(cookies(k, arr));
    }

    public static int cookies(int k, List<Integer> A) {
        int count = 0;
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for (Integer value : A)
            pq.add(value);
        if (pq.size() == 0 || pq.size() == 1)
            return -1;
        while (pq.size() > 1 && pq.peek() <= k) {
            count++;
            pq.add(pq.poll() + pq.poll() * 2);
        }
        if (pq.size() < 2 && pq.peek() < k) {
            return -1;
        }

        return count;
    }
}
