package interview.preparation.kit.warmup;

import java.util.List;

public class CountingValleys {
    public static void main(String[] args) {
        int steps = 8;
        String path = "UDDDUDUU";
        System.out.println(countingValleys(steps, path));
    }

    public static int countingValleys(int steps, String path) {
        // Write your code here
        int level = 0;
        int valley = 0;
        char[] characters = path.toCharArray();
        for (int i = 0; i < characters.length; i++)
            if (characters[i] == 'U')
                level++;
            else {
                if (level == 0)
                    valley++;
                level--;
            }
        return valley;
    }

}
