package interview.preparation.kit.warmup;

public class RepeatedString {

    public static void main(String[] args) {

        String s = "aba";
        int n = 10;

        long result = repeatedString(s, n);
        System.out.println(result);
    }

    public static long repeatedString(String s, long n) {
        long numberOfReps = n/s.length();
        long remainder = n%s.length();
        long total = 0;
        for(int a = 0; a < s.length(); a++){
            if(s.charAt(a) == 'a'){
                total++;
            }
        }
        total = total * numberOfReps;
        for(int a = 0; a < remainder; a++){
            if(s.charAt(a) == 'a'){
                total++;
            }
        }
        return total;
    }
}

//https://www.tutorialspoint.com/java-program-to-count-occurrences-of-a-word-in-string
//https://programs.programmingoneonone.com/2021/03/hackerrank-repeated-string-solution.html
