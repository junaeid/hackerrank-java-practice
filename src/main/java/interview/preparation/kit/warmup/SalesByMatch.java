package interview.preparation.kit.warmup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SalesByMatch {
    public static void main(String[] args) {
        List<Integer> ar = Arrays.asList(10, 20, 20, 10, 10, 30, 50, 10, 20);
        List<Integer> br = Arrays.asList(1, 1, 3, 1, 2, 1, 3, 3, 3, 3);
        System.out.println(sockMerchant(ar.size(), ar));
    }


    public static int sockMerchant(int n, List<Integer> ar) {
        // get unique values from the list
        List<Integer> distinctValues = ar.stream().distinct().collect(Collectors.toList());
        System.out.println(distinctValues);
        // count the unique numbers

        List<Integer> frequency_number = new ArrayList<>();

        for (int i : distinctValues) {
            frequency_number.add(Collections.frequency(ar, i));
        }
        System.out.println(frequency_number);
        // pairs++
        int count = 0;
        for (int i=0;i<frequency_number.size();i++) {
            if (frequency_number.get(i) > 1) {
                // number/2
                count += frequency_number.get(i) / 2;
            }
        }

        return count;
    }

    //https://dirask.com/posts/Java-count-number-of-element-occurrences-in-ArrayList-jQeQlj



}
