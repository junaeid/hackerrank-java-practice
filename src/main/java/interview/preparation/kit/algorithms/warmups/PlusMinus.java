package interview.preparation.kit.algorithms.warmups;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlusMinus {
    public static void main(String[] args) {
        List<Integer> arr = Arrays.asList(1, 1, 0, -1, -1);
        plusMinus(arr);
    }

    public static void plusMinus(List<Integer> arr) {
        // Write your code here
        double[] count = {0, 0, 0};
        for (Integer i : arr) {
            if (i > 0)
                count[0] += 1;
            else if (i < 0)
                count[2] += 1;
            else
                count[1] += 1;

        }

        for (int j = 0; j < count.length; j++) {
            count[j] = count[j] / arr.size();
        }
        DecimalFormat f = new DecimalFormat("##.000000");

        for (int j = 0; j < count.length; j++)
            System.out.println(f.format(count[j]));

    }
}
