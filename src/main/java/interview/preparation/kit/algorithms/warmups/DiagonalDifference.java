package interview.preparation.kit.algorithms.warmups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiagonalDifference {
    public static void main(String[] args) {
        /*
        * 11 2 4
4 5 6
10 8 -12*/
        List<List<Integer>> arr = new ArrayList<>();
        List<Integer> arr1 = Arrays.asList(11, 2, 4);
        List<Integer> arr2 = Arrays.asList(4, 5, 6);
        List<Integer> arr3 = Arrays.asList(10, 8, -12);
        arr.add(arr1);
        arr.add(arr2);
        arr.add(arr3);

        System.out.println(diagonalDifference(arr));
    }

    public static int diagonalDifference(List<List<Integer>> arr) {
        // Write your code here
        int leftSum = 0;
        int rightSum = 0;
        int n = arr.size();
        for (int i = 0; i < n; i++) {
            leftSum += arr.get(i).get(i);
            rightSum += arr.get(i).get(n - 1 - i);
        }
        return (Math.abs(leftSum - rightSum));
    }
}
