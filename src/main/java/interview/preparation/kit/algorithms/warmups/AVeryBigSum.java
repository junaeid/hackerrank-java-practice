package interview.preparation.kit.algorithms.warmups;

import java.util.ArrayList;
import java.util.List;

public class AVeryBigSum {
    public static void main(String[] args) {
        List<Long> longInteger = new ArrayList<>();
        longInteger.add(1000000001L);
        longInteger.add(1000000002L);
        longInteger.add(1000000003L);
        longInteger.add(1000000004L);
        longInteger.add(1000000005L);

        System.out.println(aVeryBigSum(longInteger));
    }

    public static long aVeryBigSum(List<Long> ar) {
        Long result = 0L;
        for (Long i : ar) {
            result += i;
        }
        return result;
    }
}
