package interview.preparation.kit.algorithms.warmups;

import java.util.Arrays;
import java.util.List;

public class BirthdayCakeCandles {

    public static void main(String[] args) {
        List<Integer> candles = Arrays.asList(3, 2, 1, 3);
        System.out.println(birthdayCakeCandles(candles));
    }

    public static int birthdayCakeCandles(List<Integer> candles) {
        // Write your code here
        int max = candles.get(0);
        int count = 0;
        for (int i = 0; i < candles.size(); i++) {

            if (max < candles.get(i)) {
                max = candles.get(i);
                count = 1;
            } else if (max == candles.get(i))
                count++;
        }

        return count;
    }
}
