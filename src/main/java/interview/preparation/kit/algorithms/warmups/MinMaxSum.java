package interview.preparation.kit.algorithms.warmups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MinMaxSum {
    public static void main(String[] args) {
        List<Integer> arr = Arrays.asList(1, 2, 3, 4, 5);
        miniMaxSum(arr);
    }

    public static void miniMaxSum(List<Integer> arr) {
        // Write your code here
        int[] sum = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++)
            for (int j = 0; j < arr.size(); j++)
                if (i != j)
                    sum[i] += arr.get(j);
        //for (int j : sum) System.out.print(j + " ");
        int[] minMax = new int[2];
        minMax[0] = sum[0];
        minMax[1] = sum[0];

        for (int i = 0; i < sum.length; i++) {
            if (minMax[0] > sum[i])
                minMax[0] = sum[i];
            if (minMax[1] < sum[i])
                minMax[1] = sum[i];

        }
        //System.out.println();
        for (int i = 0; i < minMax.length; i++) System.out.print(minMax[i] + " ");
    }

    static void miniMaxSum(int[] arr) {
        long min = 0, max = 0, sum = 0;
        min = arr[0];
        max = min;
        sum = min;
        for (int i = 1; i < arr.length; i++) {
            sum += arr[i];
            if (arr[i] < min) {
                min = arr[i];
            }
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.print((sum - max) + " " + (sum - min));
        //https://technorj.com/mini-max-sum-in-algorithm-hackerrank-programming/
    }
}
