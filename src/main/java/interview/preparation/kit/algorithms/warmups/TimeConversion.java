package interview.preparation.kit.algorithms.warmups;

public class TimeConversion {
    public static void main(String[] args) {
        String s = "12:05:45PM";
        System.out.println(timeConversion(s));
    }

    public static String timeConversion(String s) {
        // Write your code here
        String newDateTime = "";
        String[] arr = s.split(":");
        int hour = Integer.parseInt(arr[0]);
        String min = arr[1];
        String sec = arr[2].substring(0, arr[2].length() - 2);
        String period = arr[2].substring(arr[2].length() - 2, arr[2].length());

        if (period.equals("PM")) {
            if (hour != 12)
                hour += 12;
            newDateTime = hour + ":" + min + ":" + sec;
        } else {
            if (hour == 12) {
                hour = 0;
            }
            newDateTime = "0" + hour + ":" + min + ":" + sec;

        }
        return newDateTime;
    }

    //https://technorj.com/time-conversion-in-algorithm-hackerrank/
}
