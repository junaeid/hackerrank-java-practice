package interview.preparation.kit.algorithms.warmups;

public class Staircase {
    public static void main(String[] args) {
        staircase(4);
    }

    public static void staircase(int n) {
        // Write your code here
        int x = 0;
        for (int i = n; i > 0; i--) {
            for (int j = 0; j < i - 1; j++) {
                System.out.print("-");
            }
            for (int l = 0; l < x+1; l++)
                System.out.print("#");
            System.out.println();
            x++;
        }

    }
}
