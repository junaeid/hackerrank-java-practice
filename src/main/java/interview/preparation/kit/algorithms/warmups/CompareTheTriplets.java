package interview.preparation.kit.algorithms.warmups;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets {

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(5);
        a.add(6);
        a.add(7);
        List<Integer> b = new ArrayList<>();
        b.add(3);
        b.add(6);
        b.add(10);

        System.out.println(compareTriplets(a, b));
    }

    public static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<>();
        result.add(0);
        result.add(0);
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) > b.get(i))
                result.set(0, result.get(0) + 1);
            if (a.get(i) < b.get(i))
                result.set(1, result.get(1) + 1);
        }
        return result;
    }
}
