package interview.preparation.kit.algorithms.strings;

import java.util.List;

public class HackerRankInAString {
    public static void main(String[] args) {
        String s = "hackerworld";
        System.out.println(hackerrankInString(s));
    }

    public static String hackerrankInString(String s) {
        // Write your code here
        String k = "hackerrank";
        int index = 0;
        for (int i = 0; i < s.length(); i++) {
            if (index == k.length())
                break;
            if (s.charAt(i) == k.charAt(index))
                index++;
        }
        if (index == k.length())
            return "YES";
        else
            return "NO";
    }
}
