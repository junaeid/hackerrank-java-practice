package interview.preparation.kit.algorithms.strings;

public class StringTokens {

    /**
     * Matching the regular expression [A-Za-z !,?._'@]+, split the string into tokens
     * where English alphabetic letters, blank spaces, exclamation points (!), commas (,),
     * question marks (?), periods (.), underscores (_), apostrophes ('), and at symbols (@) are available.
     *
     * @param args
     */
    public static void main(String[] args) {
        String s = "He is a very very good boy, isn't he?";

        s = s.trim();
        if(s.length() == 0){
            System.out.println(0);
            return;
        }
        String[] tokens = s.split("[^A-Za-z]+");
        System.out.println(tokens.length);
        for (int i =0;i<tokens.length;i++){
            System.out.println(tokens[i]);
        }
    }
}
