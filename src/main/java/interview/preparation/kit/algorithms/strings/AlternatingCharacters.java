package interview.preparation.kit.algorithms.strings;

import org.jetbrains.annotations.NotNull;

public class AlternatingCharacters {

    public static void main(String[] args) {
        String s ="AABAAB";
        System.out.println(alternatingCharacters(s));

    }

    /**
     * String containing characters  A and B only.
     * The function will change it into a string such that there are no matching adjacent characters.
     * To do this, if there is any matching adjacent available then it will increment the counter.
     * Return the minimum number of required deletions.
     *
     * @param s string: a string containing A and B only
     * @return int: the minimum number of deletions required
     */
    public static int alternatingCharacters(@NotNull String s) {
        int i=0;
        int count = 0;
        while (i<s.length()-1){
            if(s.charAt(i)==s.charAt(i+1)){
                count++;
            }
            i++;
        }
        return count;
    }
}
