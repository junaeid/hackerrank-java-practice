package interview.preparation.kit.algorithms.strings;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class StringConstruction {
    public static void main(String[] args) {
        String s = "abcd";
        System.out.println(stringConstruction(s));
    }

    public static int stringConstruction(String s) {
        HashSet<Character> str = new HashSet<>();
        int count = 0;
        for (int i = 0; i < s.length(); i++)
            if (!str.contains(s.charAt(i))) {
                str.add(s.charAt(i));
                count++;
            }
        return count;
    }
}
