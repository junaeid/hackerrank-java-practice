package interview.preparation.kit.algorithms.strings;

public class CaesarCipher {
    public static void main(String[] args) {
        String s = "middle-Outz";
        int k = 2;
        System.out.println(caesarCipher(s, k));
    }

    public static String caesarCipher(String s, int k) {
        // Write your code here
        char sarr[] = s.toCharArray();
        for (int i = 0; i < sarr.length; i++) {
            sarr[i] = cryptIt(sarr[i], k);
        }
        return new String(sarr);
    }

    public static char cryptIt(char c, int shift) {
        if (!Character.isAlphabetic(c)) return c;
        char base = 'A';
        if (c >= 'a') base = 'a';
        return (char) (((c - base + shift) % 26) + base);
    }
}
