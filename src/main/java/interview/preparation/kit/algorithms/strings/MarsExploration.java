package interview.preparation.kit.algorithms.strings;

public class MarsExploration {
    public static void main(String[] args) {
        String s = "SOSTOT";
        System.out.println(marsExploration(s));
    }

    public static int marsExplorationAlternative(String s) {
        int numChanged = 0;
        for (int i = 0; i < s.length(); i++)
            if (i % 3 == 1)
                if (s.charAt(i) != 'O')
                    numChanged++;
                else if (s.charAt(i) != 'S')
                    numChanged++;
        return numChanged;
    }

    public static int marsExploration(String s) {
        // Write your code here
        char[] characters = s.toCharArray();
        int count = 0;
        for (int i = 0; i < characters.length; i++)
            if (characters[i] != 'S' && characters[i] != 'O')
                count++;
        return count;
    }
}
