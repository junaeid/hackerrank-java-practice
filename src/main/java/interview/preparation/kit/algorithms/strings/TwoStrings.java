package interview.preparation.kit.algorithms.strings;

import java.util.HashSet;
import java.util.Set;

public class TwoStrings {
    public static void main(String[] args) {
        String s1 = "hello";
        String s2 = "wrd";
        System.out.println(twoStrings(s1, s2));
    }

    public static String twoStrings(String s1, String s2) {
        // Write your code here
        for (int i = 0; i < s1.length(); i++)
            for (int j = 0; j < s2.length(); j++)
                if (s1.charAt(i) == s2.charAt(j))
                    return "YES";
        return "NO";
    }

    public static String twoStringsAlternative(String s1, String s2) {
        Set<Character> s1Set = new HashSet<>();
        Set<Character> s2Set = new HashSet<>();

        //creating the set of string1
        for (char c : s1.toCharArray()) {
            s1Set.add(c);
        }
        //creating the set of string2
        for (char c : s2.toCharArray()) {
            s2Set.add(c);
        }

        // store the set intersection in s1Set
        s1Set.retainAll(s2Set);

        if (!s1Set.isEmpty())
            return "YES";

        return "NO";
    }
}
