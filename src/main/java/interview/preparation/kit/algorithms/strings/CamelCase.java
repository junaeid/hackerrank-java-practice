package interview.preparation.kit.algorithms.strings;

import java.util.List;

public class CamelCase {

    public static void main(String[] args) {
        String s = "saveChangesInTheEditor";
        System.out.println(camelcase(s));
    }

    public static int camelcase(String s) {
        // Write your code here
        char[] characters = s.toCharArray();
        int count = 1;
        for (int i = 0; i < characters.length; i++) {
            if ((int) characters[i] <= 96)
                count++;
        }
        return count;
    }
}
