package interview.preparation.kit.algorithms.implementation;

public class HalloweenSale {
    public static void main(String[] args) {
        int p = 20;
        int d = 3;
        int m = 6;
        int s = 80;
        System.out.println(howManyGames(p, d, m, s));

    }

    public static int howManyGames(int p, int d, int m, int s) {
        // Return the number of games you can buy
        int priceCurr = p;
        int i=0;
        while(s>=priceCurr){
            s=s-priceCurr;
            if(priceCurr>=m+d){
                priceCurr=priceCurr-d;
            }else{
                priceCurr = m;
            }

            i++;
        }
        return i;
    }
}
