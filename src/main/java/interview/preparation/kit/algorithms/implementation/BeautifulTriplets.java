package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class BeautifulTriplets {
    public static void main(String[] args) {
        int d = 3;
        List<Integer> arr = Arrays.asList(1, 2, 4, 5, 7, 8, 10);
        System.out.println(beautifulTriplets(d, arr));
    }

    public static int beautifulTriplets(int d, List<Integer> arr) {
        // Write your code here
        int count = 0;
        for (int i = 0; i < arr.size(); i++)
            for (int j = i + 1; j < arr.size(); j++)
                if (Math.abs(arr.get(i) - arr.get(j)) == d)
                    for (int k = j + 1; k < arr.size(); k++)
                        if (Math.abs(arr.get(j) - arr.get(k)) == d)
                            count++;


        return count;
    }
}
