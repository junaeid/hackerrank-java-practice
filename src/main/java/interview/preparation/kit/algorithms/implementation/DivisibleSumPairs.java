package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class DivisibleSumPairs {
    public static void main(String[] args) {
        List<Integer> ar = Arrays.asList(1, 3, 2, 6, 1, 2);
        int k = 3;

        System.out.println(divisibleSumPairs(ar.size(), k, ar));
    }

    public static int divisibleSumPairs(int n, int k, List<Integer> ar) {
        // Write your code here
        int count = 0;
        for (int i = 0; i < ar.size() - 1; i++) {
            for (int j = i+1; j < ar.size(); j++) {
                if ((ar.get(i) + ar.get(j)) % k == 0)
                    count++;
            }
        }
        return count;
    }
}
