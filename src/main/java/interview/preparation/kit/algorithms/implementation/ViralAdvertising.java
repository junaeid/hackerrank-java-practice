package interview.preparation.kit.algorithms.implementation;

public class ViralAdvertising {
    public static void main(String[] args) {
        int n =4;
        System.out.println(viralAdvertising(n));
    }

    /**
     * Function Description Complete the viralAdvertising function in the editor below.
     *
     *
     * @param n int: the day number to report
     * @return int: the cumulative likes at that day
     */
    public static int viralAdvertising(int n) {
        int numberOfSharedPerson =5;
        int floorOfNumber =0;
        int totalNumberOfShare =0;

        for(int i =0;i<n;i++){
            floorOfNumber= numberOfSharedPerson/2;
            numberOfSharedPerson=floorOfNumber*3;
            totalNumberOfShare+=floorOfNumber;
        }
        return totalNumberOfShare;
    }
}
