package interview.preparation.kit.algorithms.implementation;


public class BeautifulDaysAtTheMovies {

    public static void main(String[] args) {
        int i = 13, j = 45, k = 3; // output 2
        System.out.println(beautifulDays(i, j, k));
    }

    public static int beautifulDays(int i, int j, int k) {
        int count = 0;
        for (int l = i; l <= j; l++) {
            if(Math.abs((l-reverseNumber(l))%k)==0)
                count++;
        }
        return count;
    }

    public static int reverseNumber(int i) {
        int reverse = 0;
        while (i != 0) {
            int remainder = i % 10;
            reverse = reverse * 10 + remainder;
            i /= 10;
        }
        return reverse;
    }
}
