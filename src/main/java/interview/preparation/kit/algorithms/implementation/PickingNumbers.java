package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class PickingNumbers {
    public static void main(String[] args) {
        List<Integer> a = Arrays.asList(4, 6, 5, 3, 3, 1);
        System.out.println(pickingNumbersAlternativeTwo(a));
    }

    /**
     * find the longest subarray where the absolute difference between any two elements
     * is less than or equal to 1.
     *
     * @param a -  takes an Array of Integers
     * @return integer of the max count of subarray
     */
    public static int pickingNumbers(List<Integer> a) {
        int max = 0;
        for (int i = 0; i < a.size(); i++) {
            int count = 0;
            for (int j = 0; j < a.size(); j++) {
                if (i == j)
                    j++;
                if (j < a.size())
                    if (Math.abs(a.get(i) - a.get(j)) <= 1)
                        count++;
                if (max < count)
                    max = count;
            }
        }
        return max;
    }

    public static int pickingNumbersAlternative(List<Integer> a) {
        int[] count = new int[100];
        int max = 0;
        for (int i = 0; i < a.size(); i++) {
            count[a.get(i)]++;
            System.out.print(count[i]);
        }
        for (int i = 0; i < 99; i++) {
            if (max < count[i] + count[i + 1]) {
                max = count[i] + count[i + 1];
            }
        }
        return max;
    }

    public static int pickingNumbersAlternativeTwo(List<Integer> a) {
        int max = 0;
        int count = 0;
        int i = 0;
        int j = 0;
        while (i != a.size()) {
            if (i == j)
                j++;
            if (j < a.size())
                if (Math.abs(a.get(i) - a.get(j)) <= 1)
                    count++;
            if (max < count)
                max = count;
            if (j >= a.size()) {
                i++;
                count = 0;
            }
            j++;
        }
        return max;
    }

}
