package interview.preparation.kit.algorithms.implementation;

import java.util.*;

public class MigratoryBirds {
    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>(Arrays.asList(1, 1, 2, 2, 3));
        System.out.println(migratoryBirds(arr));
    }

    /**
     * Takes an array and return a integer value of the most available number
     *
     * @param arr  Takes an ArrayList
     * @return Integer - return a number that is most available in the Arraylist
     * If more than 1 type has been spotted that maximum amount, return the smallest of their ids.
     */
    public static int migratoryBirds(List<Integer> arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : arr) {
            if (map.containsKey(i))
                map.put(i, map.get(i) + 1);
            else
                map.put(i, 1);
        }
        System.out.println(map);

        int maxValue = 0;
        int key = Integer.MAX_VALUE;
        for (int i : map.keySet()) {
            if (maxValue == map.get(i)) {
                if (key > i)
                    key = i;
            } else if (maxValue < map.get(i)) {
                maxValue = map.get(i);
                key = i;
            }
        }
        return key;
    }
}
