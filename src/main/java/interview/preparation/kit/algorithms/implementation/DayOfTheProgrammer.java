package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class DayOfTheProgrammer {
    public static void main(String[] args) {
        int year = 1800;
        System.out.println(dayOfProgrammer(year));
        System.out.println(leapYear(1800));
    }

    public static String dayOfProgrammer(int year) {
        List<Integer> months = Arrays.asList(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        int days = 256;
        int month = 0;
        int sum = 0;
        while (sum < days) {
            sum += months.get(month);
            month++;
        }
        String date = "";
        String monthInString = "";
        if (month < 10)
            monthInString = "0" + month;
        if (leapYear(year))
            date = (days - (sum - months.get(month + 1) + 1)) + "." + monthInString + "." + year;
        else
            date = (days - (sum - months.get(month + 1))) + "." + monthInString + "." + year;
        return date;
    }

    public static boolean leapYear(int year) {
        if (year % 100 != 0)
            return year % 4 == 0 || year % 400 == 0;
        return false;
    }

    public static String date(int y) {
        if (y == 1918) {
            return "26.09." + y;
        } else if (y >= 1919) {
            if (y % 400 == 0 || (y % 4 == 0 && y % 100 != 0)) {
                return "12.09." + y;
            } else {
                return "13.09." + y;
            }
        } else {
            if (y % 4 == 0) {
                return "12.09." + y;
            } else {
                return "13.09." + y;
            }
        }
    }
}
