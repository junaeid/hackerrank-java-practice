package interview.preparation.kit.algorithms.implementation;

import static java.lang.Math.abs;

public class CatsAndAMouse {
    public static void main(String[] args) {
        int x = 1;
        int y = 3;
        int z = 2;

        System.out.println(catAndMouse(x, y, z));
    }

    // Complete the catAndMouse function below.
    static String catAndMouse(int x, int y, int z) {
        int distanceA = 0;
        int distanceB = 0;
        distanceA = abs(x - z);
        distanceB = abs(y - z);
        if (distanceA < distanceB)
            return "Cat A";
        else if (distanceA > distanceB)
            return "Cat B";
        else
            return "Mouse C";

    }
}
