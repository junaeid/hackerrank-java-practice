package interview.preparation.kit.algorithms.implementation;

public class UtopianTree {
    public static void main(String[] args) {
        int n = 4;
        System.out.println(utopianTree(n));
    }

    public static int utopianTree(int n) {
        // Write your code here
        int height = 1;
        for (int j = 1; j <= n; j++) {
            if (j % 2 == 1)
                height *= 2;
            else
                height += 1;
        }

        return height;
    }
}
