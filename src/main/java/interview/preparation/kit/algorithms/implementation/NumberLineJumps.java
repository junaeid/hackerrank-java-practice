package interview.preparation.kit.algorithms.implementation;

public class NumberLineJumps {
    public static void main(String[] args) {
        int x1 = 0;
        int v1 = 3;
        int x2 = 4;
        int v2 = 2;

        System.out.println(kangaroo(x1, v1, x2, v2));
    }

    public static String kangaroo(int x1, int v1, int x2, int v2) {
        for (int i = x1; i <= 10000; i += x1) {
            x1 += v1;
            x2 += v2;
            if (x1 == x2)
                return "YES";
        }
        return "NO";
    }

    String kangaroo1(int x1, int v1, int x2, int v2) {

        if (v1 == v2)
            if (x1 < x2 || x2 < x1)
                return "NO";

        if (v1 < v2)
            return "NO";

        if ((x2 - x1) % (v1 - v2) == 0)
            return "YES";
        else
            return "NO";
    }
}
