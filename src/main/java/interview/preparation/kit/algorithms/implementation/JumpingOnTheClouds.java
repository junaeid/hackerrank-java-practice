package interview.preparation.kit.algorithms.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JumpingOnTheClouds {
    public static void main(String[] args) {
        List<Integer> c = new ArrayList<>(Arrays.asList(0, 0, 0, 1, 0, 0));
        System.out.println(jumpingOnClouds(c));
    }

    /**
     * Takes an array as input which initially start with zero.
     * Print the minimum number of jumps needed to win the game.<p>
     *     Time Complexity is O(n)
     * @param c an array of binary integers which always start with 0
     * @return int: the minimum number of jumps required
     */
    public static int jumpingOnClouds(List<Integer> c) {
        int count = 0;
        int i = 0;
        while (i < c.size() - 1) {
            count++;
            if (c.get(i) == 0) {
                i += 2;
                if (i >= c.size())
                    i--;
                else if (c.get(i) == 1)
                    i--;
            } else
                i++;
        }
        return count;
    }
}
