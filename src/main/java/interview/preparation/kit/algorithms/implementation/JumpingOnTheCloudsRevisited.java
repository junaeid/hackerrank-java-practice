package interview.preparation.kit.algorithms.implementation;

public class JumpingOnTheCloudsRevisited {
    public static void main(String[] args) {
        int c [] = {0, 0, 1, 0, 0, 1, 1, 0};
        int k =2;
        System.out.println(jumpingOnClouds(c,k));
    }

    /**
     * configuration of the clouds as an array , determine the final value of after the game ends.
     * @param c list of array if the cloud types along the path
     * @param k the length of one jump
     * @return int - the energy level remaining.
     */
    static int jumpingOnClouds(int[] c, int k) {
        int e=100;
        int i=0;
        while (true){
            e--;
            if (c[i]==1)
                e-=2;
            i=(i+k)%c.length;
            if(i==0)
                break;
        }
     return e;
    }
}
