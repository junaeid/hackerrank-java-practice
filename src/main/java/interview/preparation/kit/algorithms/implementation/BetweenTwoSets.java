package interview.preparation.kit.algorithms.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BetweenTwoSets {

    public static void main(String[] args) {
        List<Integer> a = Arrays.asList(2, 4);
        List<Integer> b = Arrays.asList(16, 32, 96);

        System.out.println(getTotalX(a, b));
    }

    public static int getGCD(int n1, int n2) {
        if (n2 == 0) {
            return n1;
        }
        return getGCD(n2, n1 % n2);
    }

   public static int getLCM(int n1, int n2) {
        if (n1 == 0 || n2 == 0)
            return 0;
        else {
            int gcd = getGCD(n1, n2);
            return Math.abs(n1 * n2) / gcd;
        }
    }

    public static int getTotalX(List<Integer> a, List<Integer> b) {
        int result = 0;

        // Get LCM of all elements of a
        int lcm = a.get(0);
        for (Integer integer : a) {
            lcm = getLCM(lcm, integer);
        }

        // Get GCD of all elements of b
        int gcd = b.get(0);
        for (Integer integer : b) {
            gcd = getGCD(gcd, integer);
        }

        // Count multiples of lcm that divide the gcd
        int multiple = 0;
        while (multiple <= gcd) {
            multiple += lcm;

            if (gcd % multiple == 0)
                result++;
        }

        return result;
    }
    public static int getTotalX1(List<Integer> a, List<Integer> b) {
        int count = 0;
        for (int i = (a.get(a.size() - 1)); i <= b.get(0); i++)
            for (int j = 0; j < a.size(); j++)
                if (i % a.get(j) == 0)
                    for (int k = 0; k < b.size(); k++)
                        if (i % b.get(k) == 0)
                            count++;

        return count;
    }

    public int get(List<Integer> a, List<Integer> b) {
        int x = a.get(a.size() - 1);
        List<Integer> c = new ArrayList<>();
        List<Integer> merge = new ArrayList<>();
        for (int i = x; i < b.get(0) + 1; i += x)
            c.add(i);

        merge.addAll(a);
        merge.addAll(b);
        for (int i = 0; i < c.size() - 1; i++)
            for (int j = 0; j < merge.size() - 1; j++)
                if (c.get(i) % merge.get(j) != 0)
                    c.remove(i);
        return c.size();
    }

}
