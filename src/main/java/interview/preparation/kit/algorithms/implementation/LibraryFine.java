package interview.preparation.kit.algorithms.implementation;


public class LibraryFine {
    public static void main(String[] args) {
        int d1 = 2;
        int m1 = 7;
        int y1 = 2015;
        int d2 = 1;
        int m2 = 2;
        int y2 = 2014;
        System.out.println(libraryFine(d1, m1, y1, d2, m2, y2));
    }

    public static int libraryFine(int d1, int m1, int y1, int d2, int m2, int y2) {
        // Write your code here
        if (y1 > y2)
            return 1000 * (y1 - y2);
        else if (m1 > m2 && y1 == y2)
            return 500 * (m1 - m2);
        else if (d1 > d2 && m1 == m2 && y1 == y2)
            return 15 * (d1 - d2);
        else
            return 0;
    }
}
