package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class CavityMap {
    public static void main(String[] args) {
        List<String> grid = Arrays.asList("1112", "1912", "1892", "1234");
        System.out.println(cavityMap(grid));

    }

    public static List<String> cavityMap(List<String> grid) {
        // Write your code here
        for (int i = 0; i < grid.size()-1; i++) {
            for (int j = 1; j < grid.get(i).length() - 1; j++) {
                if (grid.get(i).charAt(j) == '9') {
                    grid.set(i, grid.get(i).substring(0, j) + "X" + grid.get(i).substring(j + 1));
                }
            }
        }
        return grid;
    }

    public static List<String> cavityMapAnotherWay(List<String> grid){
        for (int i = 1; i < grid.size() - 1; i++) {
            String str = grid.get(i);
            for (int j = 1; j < grid.size() - 1; j++) {
                if (
                        str.charAt(j - 1) < str.charAt(j) &&
                        str.charAt(j) > str.charAt(j + 1) &&
                        grid.get(i - 1).charAt(j) < str.charAt(j) &&
                        str.charAt(j) > grid.get(i + 1).charAt(j)
                ) {
                    grid.set(i, grid.get(i).substring(0, j) + "X" + grid.get(i).substring(j + 1));
                }
            }
        }
        return grid;
    }
}
