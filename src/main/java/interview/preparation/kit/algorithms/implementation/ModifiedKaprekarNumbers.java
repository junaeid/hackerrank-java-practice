package interview.preparation.kit.algorithms.implementation;

import java.util.ArrayList;
import java.util.List;

public class ModifiedKaprekarNumbers {
    public static void main(String[] args) {
        int p = 1;
        int q = 100;
        kaprekarNumbers(p, q);
    }

    public static void kaprekarNumbers(int p, int q) {
        // Write your code here
        List<Integer> arr = new ArrayList<>();
        String a = null, b = null;
        for (int i = p; i < q; i++) {
            String j = Integer.toString(i * i);
            if (j.length() > 1) {
                a = j.substring(0, j.length() / 2);
                b = j.substring(j.length() / 2);
            }
            if (i == 1)
                arr.add(i);
            if (a != null && i == Integer.parseInt(a) + Integer.parseInt(b)) {
                arr.add(i);
            }

        }
        for (int i = 0; i < arr.size(); i++)
            System.out.print(arr.get(i) + " ");
    }
}
