package interview.preparation.kit.algorithms.implementation;

import com.sun.source.tree.ReturnTree;

import java.util.Arrays;
import java.util.List;

public class SubarrayDivision {
    public static void main(String[] args) {
        List<Integer> s = Arrays.asList(1, 2, 1, 3, 2);
        int d = 3;
        int m = 2;

        System.out.println(birthday(s, d, m));

    }

    public static int birthday(List<Integer> s, int d, int m) {
        // Write your code here
        int result = 0;
        for (int i = 0; i < s.size() - m + 1; i++) {
            int tmpSum = 0;
            for (int j = i; j < i + m; j++)
                tmpSum += s.get(j);
            if (tmpSum == d)
                result++;
        }
        return result;
    }
}
