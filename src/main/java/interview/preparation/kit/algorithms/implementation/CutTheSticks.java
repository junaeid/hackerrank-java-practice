package interview.preparation.kit.algorithms.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CutTheSticks {

    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>(Arrays.asList(5, 4, 4, 2, 2, 8));
        System.out.println(cutTheSticks(arr));
    }

    /**
     * A number of sticks of varying lengths. Iteratively cut the sticks into smaller sticks,
     * discarding the shortest pieces until there are none left.
     * Returns int[]: the number of sticks after each iteration
     *
     * @param arr the lengths of sticks
     * @return int[]: the number of sticks after each iteration
     */
    public static List<Integer> cutTheSticks(List<Integer> arr) {
        List<Integer> sizeOfArray = new ArrayList<>();
        Collections.sort(arr);
        while (!arr.isEmpty()) {
            sizeOfArray.add(arr.size());
            int temp = arr.get(0);
            for (int i = 0; i < arr.size(); i++) {
                if (arr.get(i) == temp) {
                    arr.remove(i);
                    i--;
                } else arr.set(i, arr.get(i) - temp);

            }
        }
        return sizeOfArray;

    }
}
