package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class AppleAndOrange {
    public static void main(String[] args) {
        int s = 7;
        int t = 11;
        int a = 5;
        int b = 15;
        List<Integer> apples = Arrays.asList(-2, 2, 1);
        List<Integer> oranges = Arrays.asList(5, -6);

        countApplesAndOranges(s, t, a, b, apples, oranges);
    }

    public static void countApplesAndOranges(int s, int t, int a, int b, List<Integer> apples, List<Integer> oranges) {
        // Write your code here
        apples = getActualLocation(a, apples);
        oranges = getActualLocation(b, oranges);
        System.out.println(apples);
        System.out.println(getNumberOfFruits(s, t, apples));
        System.out.println(getNumberOfFruits(s, t, oranges));

    }

    public static List<Integer> getActualLocation(int tree, List<Integer> fruits) {
        for (int i = 0; i < fruits.size(); i++)
            fruits.set(i, fruits.get(i) + tree);
        return fruits;
    }

    public static int getNumberOfFruits(int s, int t, List<Integer> fruits) {
        int count = 0;
        for (int i = 0; i < fruits.size(); i++)
            if (fruits.get(i) >= s && fruits.get(i) <= t)
                count++;
        return count;
    }
}
