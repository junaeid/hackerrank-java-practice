package interview.preparation.kit.algorithms.implementation;

public class SherlockAndSquares {
    public static void main(String[] args) {
        int a = 24;
        int b = 49;
        System.out.println(squaresAlternative(a, b));
    }

    /**
     * Return an integer representing the number of square integers
     * in the inclusive range from a to b. Time Complexity O(n)
     * @param a int: the lower range boundary
     * @param b int: the upper range boundary
     * @return int: the number of square integers in the range
     */
    public static int squares(int a, int b) {
        int count =0;
        for(int i = a;i<=b;i++){
            if(Math.sqrt(i)%1==0)
                count++;
        }
        return count;
    }

    /**
     * Return an integer representing the number of square integers
     * in the inclusive range from a to b. Time Complexity O(n)
     * @param a int: the lower range boundary
     * @param b int: the upper range boundary
     * @return int: the number of square integers in the range
     */
    public static int squaresAlternative(int a,int b){
        int count=0;
        int sqr=1;
        int sum=3;

        while (sqr<=b)
        {
            if(sqr>=a)
                count ++;
            sqr=sqr+sum;
            sum=sum+2;
        }

        return count;


    }
}
