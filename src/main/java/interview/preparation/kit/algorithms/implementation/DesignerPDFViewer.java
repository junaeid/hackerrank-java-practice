package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class DesignerPDFViewer {
    public static void main(String[] args) {
        List<Integer> h = Arrays.asList(1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5);
        String word = "abc";
        System.out.println(designerPdfViewer(h, word));
    }

    public static int designerPdfViewer(List<Integer> h, String word) {
        // Write your code here
        int mx = 0;
        for (int i = 0; i < word.length(); i++) {
            int f = h.get(word.charAt(i) - 'a');
            if (f > mx) {
                mx = f;
            }
        }
        return mx * word.length();
    }

}
