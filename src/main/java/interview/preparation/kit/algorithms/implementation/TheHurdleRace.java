package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class TheHurdleRace {
    public static void main(String[] args) {
        int k = 4;
        List<Integer> height = Arrays.asList(1, 6, 3, 5, 2);
        System.out.println(hurdleRace(4, height));
    }

    public static int hurdleRace(int k, List<Integer> height) {
        // Write your code here
        int max = 0;
        for (int i = 0; i < height.size(); i++)
            if (max < height.get(i))
                max = height.get(i);
        if (k < max)
            return max - k;
        return 0;
    }
}
