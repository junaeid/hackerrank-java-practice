package interview.preparation.kit.algorithms.implementation;

/**
 *
 */
public class FindDigits {
    public static void main(String[] args) {
        int n = 124;
        System.out.println(findDigits(n));
    }

    /**
     * Returns the number of divisors occurring within the integer.
     *
     * @param n an integer
     * @return Integer - the number of divisors
     */
    public static int findDigits(int n) {
        int temp;
        int ne = n;
        int count = 0;
        while (ne > 0) {
            temp = ne % 10;
            if (ne % 10 != 0 && n % temp == 0)
                count++;
            ne /= 10;
        }
        return count;
    }

}
