package interview.preparation.kit.algorithms.implementation;

public class ElectronicsShop {
    public static void main(String[] args) {

        int[] keyboards = {5, 1,1};
        int[] drives = {4};
        int b = 5;

        System.out.println(getMoneySpent(keyboards, drives, b));
    }

    static int getMoneySpent(int[] keyboards, int[] drives, int b) {
        /*
         * Write your code here.
         */
        int max = 0;
        for (int i = 0; i < keyboards.length; i++)
            for (int j = 0; j < drives.length; j++) {
                int bill = keyboards[i] + drives[j];
                if (max < bill && b >= bill)
                    max = bill;
            }
        if (max > 0)
            return max;
        else
            return -1;
    }
}
