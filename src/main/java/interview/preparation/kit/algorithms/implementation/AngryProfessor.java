package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class AngryProfessor {
    public static void main(String[] args) {
        int k = 4;
        List<Integer> a = Arrays.asList(-1, -3, 4, 2);
        System.out.println(angryProfessor(k, a));
    }

    public static String angryProfessor(int k, List<Integer> a) {
        // Write your code here
        int count = 0;
        for (int i = 0; i < a.size(); i++)
            if (a.get(i) <= 0)
                count++;
        if (count >= k)
            return "NO";
        else
            return "YES";

    }
}
