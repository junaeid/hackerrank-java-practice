package interview.preparation.kit.algorithms.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceLane {
    public static void main(String[] args) {

        List<Integer> width = new ArrayList<>(Arrays.asList(2, 3, 2, 1));
        List<List<Integer>> cases = new ArrayList<>();

        // populating the 2d array started
        List<Integer> r1 = new ArrayList<>(Arrays.asList(1, 2));
        cases.add(r1);
        List<Integer> r2 = new ArrayList<>(Arrays.asList(2, 4));
        cases.add(r2);
        // populating the 2d array ended

        // output of 2d array
        System.out.println("2d array started");
        for (int i = 0; i < cases.size(); i++) {
            for (int j = 0; j < cases.get(i).size(); j++) {
                System.out.print(cases.get(i).get(j) + " ");
            }
            System.out.println();
        }
        System.out.println("2d array ended\n");


        System.out.println("output of serviceLane");
        System.out.println(serviceLane(width, cases));

    }

    /**
     * The first line of input contains two integers, n and t, where n denotes the number of width measurements  and t,
     * the number of test cases. The next line has n space-separated integers which represent the array width.
     * The next t lines contain two integers, i and j, where i is the start index and j is the end index of the segment
     * to check.
     *
     * @param width int n: the size of the width array
     * @param cases int cases[t][2]: each element contains the starting and ending indices for a segment to consider,
     *              inclusive
     * @return int[t]: the maximum width vehicle that can pass through each segment of the service lane described
     */
    public static List<Integer> serviceLane(List<Integer> width, List<List<Integer>> cases) {
        List<Integer> minimunValueList = new ArrayList<>();
        for (int i = 0; i < cases.size(); i++) {
            int min = Integer.MAX_VALUE;
            for (int j = cases.get(i).get(0); j < cases.get(i).get(1); j++) {
                if (min > width.get(j)) {
                    min = width.get(j);
                }
                minimunValueList.add(min);
            }
        }
        return minimunValueList;
    }
}
