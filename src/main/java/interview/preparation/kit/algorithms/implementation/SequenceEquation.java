package interview.preparation.kit.algorithms.implementation;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SequenceEquation {
    public static void main(String[] args) {
        List<Integer> p = Arrays.asList(4, 3, 5, 1, 2);
        System.out.println(permutationEquation(p));
    }

    /**
     * Takes a sequence of n integers as p, where each element is distinct and satisfies 1<= p(x) <= p.size() .
     * For each x where 0 <=a  x < p.size(), that is increments from 1 to p.size(), returns integer
     * such that p(p(y))=~x and keep a history of the values of y in a return array.
     * @param p ArrayList as an Input
     * @return ArrayList of Integer sequence
     */
    public static @NotNull List<Integer> permutationEquation(@NotNull List<Integer> p) {
        List<Integer> sequence = new ArrayList<>();

        for (int i = 1; i <= p.size(); i++) {
            int position = getPosition(p, i);
            int positionOne = getPosition(p, position);
            sequence.add( positionOne);
            System.out.println(sequence);
        }
        return sequence;
    }

    /**
     * Takes a sequence of n integers as p and returns the position of the value in that array
     * @param p ArrayList as an Input
     * @param value Integer: that is available in the ArrayList  variable p
     * @return Integer: position of the number
     */
    private static int getPosition(@NotNull List<Integer> p, int value) {
        int positionOne = 0;
        for (int k = 0; k < p.size(); k++)
            if (value == p.get(k)) {
                positionOne = k + 1;
                break;
            }
        return positionOne;
    }
}
