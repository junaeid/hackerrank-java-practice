package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MinimumDistances {
    public static void main(String[] args) {
        List<Integer> a = Arrays.asList(7, 1, 3, 4, 1, 7);
        System.out.println(minimumDistances(a));

    }

    public static int minimumDistances(List<Integer> a) {
        // Write your code here
        int min = 10 ^ 5 + 1;
        for (int i = 0; i < a.size(); i++)
            for (int j = i + 1; j < a.size(); j++)
                if (a.get(i) == a.get(j))
                    if (Math.abs(i - j) < min)
                        min = Math.abs(i - j);

        return (min == (10 ^ 5 + 1)) ? -1 : min;
    }

    public void minDistanceUsingHashmap(List<Integer> A) {
        HashMap<Integer, Integer> distances = new HashMap<>();

        int minDistance = -1;

        for (int i = 0; i < A.size(); i++) {
            if (distances.containsKey(A.get(i))) {
                //Calculate distance between like numbers
                int distance = Math.abs(distances.get(A.get(i)) - i);

                if (distance < minDistance || minDistance == -1) {
                    minDistance = distance;
                }

                //Set start point to the old end point
                distances.put(A.get(i), i);
            } else {
                //Add a new starting point
                distances.put(A.get(i), i);
            }
        }

        System.out.println(minDistance);
    }
}
