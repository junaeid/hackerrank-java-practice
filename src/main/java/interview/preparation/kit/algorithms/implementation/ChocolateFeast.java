package interview.preparation.kit.algorithms.implementation;

public class ChocolateFeast {
    public static void main(String[] args) {
        int n = 15;
        int c = 3;
        int m = 2;

        System.out.println(chocolateFeast(n, c, m));
    }

    /**
     * Returns the total number of Chocolate Bar that can be possible to get.<p>
     * The Time Complexity is O(n)
     *
     * @param n  initial amount of money
     * @param c  the cost of a chocolate bar
     * @param m  the number of wrappers can turn in for a free bar
     * @return Integer -  total number of chocolate can get
     */
    public static int chocolateFeast(int n, int c, int m) {
        int chocolateBar = 0;
        int wrapper = 0;
        wrapper = n / c;
        chocolateBar += wrapper;
        int i = wrapper / m;
        while (i > 0) {
            i = wrapper / m;
            chocolateBar += i;
            wrapper %= m;
            wrapper += i;
        }
        return chocolateBar;
    }
}
