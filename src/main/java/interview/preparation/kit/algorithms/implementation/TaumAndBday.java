package interview.preparation.kit.algorithms.implementation;

public class TaumAndBday {
    public static void main(String[] args) {
        int b = 7;
        int w = 7;
        int bc = 4;
        int wc = 2;
        int z = 1;
        System.out.println(taumBday(b, w, bc, wc, z));

    }

    /**
     * Takes number of black gifts, white gifts and their cost, conversion between the cost of both color.
     * Returns the minimum cost to purchase the gift.
     *
     * @param b  int: the number of black gifts
     * @param w  int: the number of white gifts
     * @param bc int: the cost of a black gift
     * @param wc int: the cost of a white gift
     * @param z  int: the cost to convert one color gift to the other color
     * @return long: the minimum cost to purchase the gifts
     */
    public static long taumBday(int b, int w, int bc, int wc, int z) {
        long cost = 0;
        if (bc < wc) {
            if (bc + z < wc)
                wc = bc + z;
        } else {
            if (wc + z < bc)
                bc = wc + z;
        }
        cost = (long) b * bc + (long) w * wc;
        return cost;
    }
}
