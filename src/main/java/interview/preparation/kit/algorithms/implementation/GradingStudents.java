package interview.preparation.kit.algorithms.implementation;

import java.util.Arrays;
import java.util.List;

public class GradingStudents {
    public static void main(String[] args) {
        List<Integer> grades = Arrays.asList(73, 67, 38, 33);
        System.out.println(gradingStudents(grades));
    }

    public static List<Integer> gradingStudentsOne(List<Integer> grades) {
        // Write your code here
        for (int i = 0; i < grades.size(); i++) {
            int grade = grades.get(i);
            if (grade > 33 && grade % 10 != 0) {
                int divisor = ((grade / 5) + 1) * 5;
                if (divisor - grade < 3)
                    grades.set(i, divisor);
            }
        }
        return grades;
    }

    public static List<Integer> gradingStudents(List<Integer> grades) {
        // Write your code here
        for (int a0 = 0; a0 < grades.size(); a0++) {
            int grade = grades.get(a0);
            if (grade >= 38) {
                if (grade % 5 >= 3) grades.set(a0, (grade / 5) * 5 + 5);
            }

        }
        return grades;
    }

}
