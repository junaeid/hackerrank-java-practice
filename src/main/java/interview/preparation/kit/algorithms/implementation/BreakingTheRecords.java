package interview.preparation.kit.algorithms.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BreakingTheRecords {
    public static void main(String[] args) {
        List<Integer> scores = Arrays.asList(10, 5, 20, 20, 4, 5, 2, 25, 1);

        System.out.println(breakingRecords(scores));
    }

    public static List<Integer> breakingRecords(List<Integer> scores) {
        // Write your code here
        int minimum = scores.get(0);
        int maximum = scores.get(0);
        List<Integer> maxMin = Arrays.asList(0, 0);

        for (int i = 1; i < scores.size(); i++) {
            if (scores.get(i) > maximum) {
                maximum = scores.get(i);
                maxMin.set(0, maxMin.get(0) + 1);
            } else if (scores.get(i) < minimum) {
                minimum = scores.get(i);
                maxMin.set(1, maxMin.get(1) + 1);
            }
        }
        return maxMin;
    }
}
