package interview.preparation.kit.algorithms.sorting;

import java.util.Arrays;
import java.util.List;

public class InsertionSortPart2 {
    public static void main(String[] args) {
        List<Integer> arr = Arrays.asList(1, 4, 3, 5, 6, 2);
        insertionSort2(arr.size(), arr);
    }

    public static void insertionSort2(int n, List<Integer> arr) {
        // Write your code here
        for (int i = 1; i < n; i++) {
            int temp = arr.get(i);
            for (int j = i - 1; j >= 0 && temp < arr.get(j); j--) {
                arr.set(j + 1, arr.get(j));
                arr.set(j, temp);
            }
            for (int j = 0; j < n; j++)
                System.out.print(arr.get(j) + " ");
            System.out.println();
        }
    }

    public static void printArray(List<Integer> arr) {
        for (int j = 0; j < arr.size(); j++)
            System.out.print(arr.get(j) + " ");
        System.out.println();
    }
}
