package interview.preparation.kit.algorithms.sorting;

import java.util.Arrays;
import java.util.List;

public class IntroToTutorialChallenges {
    public static void main(String[] args) {
        int v = 4;
        List<Integer> arr = Arrays.asList(1, 4, 5, 7, 9, 12);
        System.out.println(introTutorial(v, arr));
    }

    public static int introTutorial(int v, List<Integer> arr) {
        // Write your code here
        for (int i = 0; i < arr.size(); i++)
            if (v == arr.get(i))
                return i;
        return 0;
    }
}
