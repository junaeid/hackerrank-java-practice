package interview.preparation.kit.algorithms.sorting;

import java.util.Arrays;
import java.util.List;

public class InsertionSortPart1 {
    public static void main(String[] args) {
        List<Integer> arr = Arrays.asList(2, 4, 6, 8, 3);
        insertionSort1(arr.size(), arr);

    }

    public static void insertionSort1(int n, List<Integer> arr) {
        // Write your code here
        int temp = 0;
        for (int i = n - 1; i > 0; i--) {
            temp = arr.get(i);

            if (arr.get(i - 1) > temp) {
                arr.set(i, arr.get(i - 1));
                printArray(arr);
                arr.set(i - 1, temp);
            }

        }
        printArray(arr);

    }

    public static void printArray(List<Integer> arr) {
        for (int j = 0; j < arr.size(); j++)
            System.out.print(arr.get(j) + " ");
        System.out.println();
    }
}
