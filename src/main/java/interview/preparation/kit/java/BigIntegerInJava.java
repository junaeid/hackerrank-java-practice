package interview.preparation.kit.java;

import java.math.BigInteger;
import java.util.Scanner;

public class BigIntegerInJava {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        BigInteger A,B;
        A = new BigInteger(sc.next());
        B = new BigInteger(sc.next());

        System.out.println(A.add(B));
        System.out.println(A.multiply(B));

    }
}
