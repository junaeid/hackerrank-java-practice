package interview.preparation.kit.java;

import java.io.IOException;
import java.math.BigInteger;

public class PrimalityTest {
    public static void main(String[] args) throws IOException {

        BigInteger bi = new BigInteger(String.valueOf(10));
        System.out.println(bi.isProbablePrime(10) ? "prime" : "not prime");

    }
    boolean isPrime(long n) {
        if(n < 2) return false;
        if(n == 2 || n == 3) return true;
        if(n%2 == 0 || n%3 == 0) return false;
        long sqrtN = (long)Math.sqrt(n)+1;
        for(long i = 6L; i <= sqrtN; i += 6) {
            if(n%(i-1) == 0 || n%(i+1) == 0) return false;
        }
        return true;
    }
}
