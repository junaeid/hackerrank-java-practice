package interview.preparation.kit.arrays;

import com.sun.security.jgss.GSSUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LeftRotation {
    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        for (int i = 1; i < 6; i++)
            a.add(i);
        int d = 4;
        System.out.println(rotLeft(a, d));
    }

    public static List<Integer> rotLeft(List<Integer> a, int d) {
        for (int i = 0; i < d; i++) {
            //get the last value from the arraylist
            int temp = a.get(0);
            //System.out.println(temp);
            //move everything to left side
            for (int j = 0; j < a.size() - 1; j++)
                a.set(j, a.get(j + 1));
            //put the last value to the first position
            a.set(a.size() - 1, temp);
        }
        return a;
    }

    static List<Integer> rotLeftArray(List<Integer> a, int d) {
        int n = a.size();
        List<Integer> rotArray = new ArrayList<>();

        for (int oldIndex = 0; oldIndex < n; oldIndex++) {
            int newIndex = (oldIndex + n - d) % n;
            rotArray.set(newIndex, a.get(oldIndex));
        }

        return rotArray;
    }
}
